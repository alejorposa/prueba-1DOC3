

def solve(problem)
  "#{solve_for_smallest(problem.dup)} #{solve_for_largest(problem.dup)}"
end

def solve_for_smallest(problem)

  index = 0
  problem.each_char do |nr|

    subproblem = problem[index...problem.length].split("").map(&:to_i)

    min = subproblem.min

    if min < nr.to_i && !(index == 0 && min == 0)

      subindex = 0
      problem.reverse.each_char do |subnr|
        if subnr.to_i == min
          break
        end
        subindex += 1
      end

      replace_index = problem.length-1-subindex

      problem[replace_index] = nr.to_s
      problem[index] = min.to_s

      break
    end

    index += 1
  end

  "#{problem}"
end


def solve_for_largest(problem)

  index = 0
  problem.each_char do |nr|

    subproblem = problem[index...problem.length].split("").map(&:to_i)

    max = subproblem.max

    if max > nr.to_i

      subindex = 0
      problem.reverse.each_char do |subnr|
        if subnr.to_i == max
          break
        end
        subindex += 1
      end

      replace_index = problem.length-1-subindex

      problem[replace_index] = nr.to_s
      problem[index] = max.to_s

      break
    end

    index += 1
  end

  "#{problem}"
end

# Read input
problems = File.readlines("prueba.txt")
T = problems.shift

count = 1
while problems.any?
  problem = problems.shift
  
  puts "Case ##{count}: " + solve(problem.strip)

  count += 1
end
