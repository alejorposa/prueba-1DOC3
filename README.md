# Running

`ruby cooking_the_books.rb`

TL;DR: For the ith number, print a line containing "Case #i: " followed by the smallest and largest numbers that can be made from the original number N, using at most a single swap.

